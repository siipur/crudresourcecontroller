@extends('posts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3>Simple Laravel CRUD with resource controller</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div sclass="pull-left">
                <h5>Edit Post</h5>
                <hr/>
            </div>
        </div>
    </div>


    @if (count($errors)>0)
        <div class="alert alert-danger">
            <strong>Whoooop!!!</strong>, There where problem with your's input<br/>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    {!! Form::model($post, ['method'=>'PATCH','route'=>['posts.update',$post->id]]) !!}
    @include('posts.form');
    {!! Form::close() !!}

@endsection