@extends('posts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h3>Simple Laravel CRUD with resource controller</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div sclass="pull-right">
                <a class="btn btn-primary" href="{{ route('posts.create') }}" role="button">Create New Post</a>    
            </div>
        </div>
    </div>


    @if($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No.</th>
            <th>Title</th>
            <th>Body</th>
            <th width="300px">Action</th>
        </tr>
        @foreach ($posts as $post)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $post->title }}</td>
            <td>{{ $post->body }}</td> 
            <td>
                <a class="btn btn-primary" href="{{ route('posts.show', $post->id) }}" role="button">Show</a>
                <a class="btn btn-warning" href="{{ route('posts.edit', $post->id) }}" role="button">Edit</a>
                {!! Form::open(['method' => 'DELETE', 'route'=>['posts.destroy', $post->id], 'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class'=>'btn btn-xs btn-danger']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </table>
    
    {!! $posts->render() !!}

    @endsection