<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <strong>Title: </strong>
            {!! form::text('title',null, ['placeholder'=>'isi title','class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            <strong>Body: </strong>
            {!! form::textarea('body',null, ['placeholder'=>'isi body','class'=>'form-control','style'=>'height:150px']) !!}
        </div>
        <a class="btn btn-warning" href="{{ route('posts.index')}}">Back</a>
        <button type="submit" class="btn btn-success">Save</button>
    </div>
</div>